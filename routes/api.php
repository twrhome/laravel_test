<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix("v1")->group(function () {
	Route::get('/get_links', 'LinkController@get_links');
    Route::post('/create_link', 'LinkController@create_link');
    Route::post('/update_link/{id}', 'LinkController@update_link');
    Route::post('/delete_link', 'LinkController@delete_link');
});

