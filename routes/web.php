<?php

use App\link;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'LinkController@index');


Route::post('/addLink', 'LinkController@store');

Route::post('/delLink', 'LinkController@destroy');



// Route::post('/addLink', function (Request $request) {
// 	// dd($request->all());
//     $validator = Validator::make($request->all(), [
//         'name' => 'required|max:255',
//     ]);

//     if ($validator->fails()) {
//         return redirect('/')
//             ->withInput()
//             ->withErrors($validator);
//     }

//     $link = new link;
//     $link->name 	= $request->name;
//     $link->img_path = $request->path;
//     $link->save();

//     return redirect('/home');
// });
