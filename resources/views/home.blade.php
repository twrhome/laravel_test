@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="/addLink" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <!-- 任務名稱 -->
                        <div class="form-group">
                            <label for="task" class="col-sm-3 control-label">Link Name</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" class="form-control">
                            </div>

                            <br>
                            <!-- <label for="task" class="col-sm-3 control-label">Link path</label> -->

                            <input name="file" type="file" accept="image/*" value=""/>

                           <!--  <div class="col-sm-6">
                                <input type="text" name="path" class="form-control">
                            </div> -->
                        </div>

                        <!-- 增加任務按鈕-->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-plus"></i> 增加任務
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- You are logged in! -->
                </div>
            </div>
        @if (count($links) > 0)
            <div class="panel panel-default">
          

                <div class="panel-body">
                    <table class="table table-bordered">

                        <!-- 表頭 -->
                        <thead>
                            <th>名稱</th>
                            <th>圖片</th>
                            <th>建立時間</th>
                            <th>操作</th>

                        </thead>

                        <!-- 表身 -->
                        <tbody>
                            @foreach ($links as $link)
                                <tr>
                                    <!-- 任務名稱 -->
                                    <td class="table-text">
                                        <div>{{ $link->name }}</div>
                                    </td>

                                    <td class="table-text">
                                        <img src="{{ url('/').$link->img_path }}" alt="..." class="img-rounded" style="width:600px">
                                        
                                    </td>

                                    <td class="table-text">
                                        <div>{{ $link->created_at }}</div>
                                    </td>

                                    <td>
                                        <form action="/delLink" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{ $link->id }}">
                                            <button type="submit" class="btn btn-danger">刪除任務</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif

            
        </div>
    </div>
</div>
@endsection
