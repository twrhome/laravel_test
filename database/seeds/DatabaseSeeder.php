<?php

use Illuminate\Database\Seeder;
use App\Http\Controllers\Auth\UserModel;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(UserModel::class, 5)->create();
    }
}
