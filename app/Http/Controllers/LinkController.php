<?php

namespace App\Http\Controllers;

use App\link;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests\LinkPostRequest;
use Illuminate\Support\Facades\Storage;

class LinkController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = link::orderBy('created_at', 'asc')->get();

        return view('home', [
            'links' => $links
        ]);
        // return view('home');
    }

    // 得到所有link的 api
    public function get_links()
    {
        $links = link::orderBy('created_at', 'asc')->get();

        // return $links;
        return response()->json($links, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // 新增link的 api
    public function create_link(Request $request)
    {
        $link = link::create($request->all());
        return response()->json($link, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $validated = $request->validated();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        if($request->hasFile('file')){
            $image = $request->file('file');
            $file_path = $image->store('public');
        }

        $link = new link;
        $link->name  = $request->name;
        $link->img_path = Storage::url($file_path);
        $link->save();

        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\link  $link
     * @return \Illuminate\Http\Response
     */
    public function show(link $link)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\link  $link
     * @return \Illuminate\Http\Response
     */
    public function edit(link $link)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\link  $link
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, link $link)
    {

    }

     // 更新link的 api
    public function update_link($postId, Request $request)
    {
        $link = link::where('id', $postId)->update($request->all());

        return response()->json($link, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\link  $link
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $link = link::find($request->id);

        $link->delete();

        return redirect('/home'); 
    }

     // 刪除link的 api
    public function delete_link(Request $request)
    {
        $link = link::destroy($request->id);
        return response()->json($link, 200);
    }

}
