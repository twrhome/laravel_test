<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];


    /**
     * Mutator Password
     *
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes[ 'password' ] = bcrypt($value);
    }
}


